/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.additionaltask2;
import java.util.Scanner;
/**
 *
 * @author spoty
 */
public class AdditionalTask2 {

    public static void main(String[] args) {
     Scanner scanner = new Scanner(System.in); 
     System.out.println("Write x1");
     float x1 = scanner.nextFloat();
     System.out.println("Write y1");
     float y1 = scanner.nextFloat();
     System.out.println("Write x2");
     float x2 = scanner.nextFloat();
     System.out.println("Write y2");
     float y2 = scanner.nextFloat();
     System.out.println("distance between two points: " + Math.sqrt(((x1-x2)*(x1-x2))+((y1-y2)*(y1-y2)))); 
    }
}
